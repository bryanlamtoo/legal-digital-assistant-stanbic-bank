<?php
$edit_data		=	$this->db->get_where('question_category' , array('category_id' => $param2) )->row();

	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title" >
						<i class="entypo-plus-circled"></i>
						<?php echo get_phrase('edit_subject');?>
					</div>
				</div>
				<div class="panel-body">
					<?php echo form_open(base_url() . 'index.php?admin/question_category/do_update/'.$edit_data->category_id , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
					<div class="form-group">
						<label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
						<div class="col-sm-5 controls">
							<input type="text" class="form-control" name="name" value="<?php echo $edit_data->name;?>"/>
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo get_phrase('edit_category');?></button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>




