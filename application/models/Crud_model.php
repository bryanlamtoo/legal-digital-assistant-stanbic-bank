<?php

/**
 * Created by IntelliJ IDEA.
 * User: bryanlamtoo
 * Date: 20/07/2017
 * Time: 10:29
 */

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Crud_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function clear_cache()
	{
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}

//	function get_type_name_by_id($type, $type_id = '', $field = 'name')
//	{
//		$this->db->where($type . '_id', $type_id);
//		$query = $this->db->get($type);
//		$result = $query->result_array();
//		foreach ($result as $row)
//			return $row[$field];
//		//return	$this->db->get_where($type,array($type.'_id'=>$type_id))->row()->$field;
//	}


	function get_type_name_by_id($type, $type_id = '', $field = 'name') {
		return $this->db->get_where($type, array($type . '_id' => $type_id))->row()->$field;
	}

	function save_user_info()
	{
		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		$data['password'] = sha1($this->input->post('password'));
		$data['address'] = $this->input->post('address');
		$data['phone'] = $this->input->post('phone');

		$this->db->insert('user', $data);

		$user_id = $this->db->insert_id();
		move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/user_image/" . $user_id . '.jpg');
	}

	function select_user_info()
	{
		return $this->db->get('user')->result_array();
	}


	function update_own_info($admin_id)
	{

		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		$data['user_name'] = $this->input->post('username');

		$this->db->where('admin_id', $this->session->userdata('login_user_id'));
		$this->db->update('admin', $data);

		move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/admin_image/" . $admin_id . '.jpg');

	}

	function update_user_info($user_id)
	{
		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		$data['address'] = $this->input->post('address');
		$data['phone'] = $this->input->post('phone');

		$this->db->where('user_id', $user_id);
		$this->db->update('user', $data);

		move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/user_image/" . $user_id . '.jpg');
	}

	function delete_user_info($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete('user');
	}


	////////IMAGE URL//////////
	function get_image_url($type = '', $id = '')
	{
		if (file_exists('uploads/' . $type . '_image/' . $id . '.jpg'))
			$image_url = base_url() . 'uploads/' . $type . '_image/' . $id . '.jpg';
		else
			$image_url = base_url() . 'uploads/user.jpg';

		return $image_url;
	}


	function save_notice_info()
	{
		$data['title'] = $this->input->post('title');
		$data['description'] = $this->input->post('description');
		if ($this->input->post('start_timestamp') != '')
			$data['start_timestamp'] = strtotime($this->input->post('start_timestamp'));
		else
			$data['start_timestamp'] = '';
		if ($this->input->post('end_timestamp') != '')
			$data['end_timestamp'] = strtotime($this->input->post('end_timestamp'));
		else
			$data['end_timestamp'] = $data['start_timestamp'];

		$this->db->insert('notice', $data);
	}

	function save_question_info()
	{

		$data['title'] = $this->input->post('question_title');
		$data['question'] = $this->input->post('question_content');
		$data['author_id'] = $this->session->userdata('login_user_id');

		$this->db->insert('question', $data);
	}


	function save_answer_info($question_id)
	{

		$data['answer'] = $this->input->post('answer');
		$data['question_id'] = $question_id;
		$data['timestamp'] = time();
		$data['author_id'] = $this->session->userdata('login_user_id');

		$this->db->insert('answer', $data);
	}

	function select_notice_info()
	{
		return $this->db->get('notice')->result_array();
	}

	function select_question_info()
	{
		return $this->db->get('question')->result_array();
	}

	function update_notice_info($notice_id)
	{
		$data['title'] = $this->input->post('title');
		$data['description'] = $this->input->post('description');
		if ($this->input->post('start_timestamp') != '')
			$data['start_timestamp'] = strtotime($this->input->post('start_timestamp'));
		else
			$data['start_timestamp'] = '';
		if ($this->input->post('end_timestamp') != '')
			$data['end_timestamp'] = strtotime($this->input->post('end_timestamp'));
		else
			$data['end_timestamp'] = $data['start_timestamp'];

		$this->db->where('notice_id', $notice_id);
		$this->db->update('notice', $data);
	}

	function delete_notice_info($notice_id)
	{
		$this->db->where('notice_id', $notice_id);
		$this->db->delete('notice');
	}


	//////system settings//////
	function update_system_settings()
	{
		$data['description'] = $this->input->post('system_name');
		$this->db->where('type', 'system_name');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('system_title');
		$this->db->where('type', 'system_title');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('address');
		$this->db->where('type', 'address');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('phone');
		$this->db->where('type', 'phone');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('paypal_email');
		$this->db->where('type', 'paypal_email');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('currency');
		$this->db->where('type', 'currency');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('system_email');
		$this->db->where('type', 'system_email');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('buyer');
		$this->db->where('type', 'buyer');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('system_name');
		$this->db->where('type', 'system_name');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('purchase_code');
		$this->db->where('type', 'purchase_code');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('language');
		$this->db->where('type', 'language');
		$this->db->update('settings', $data);

		$data['description'] = $this->input->post('text_align');
		$this->db->where('type', 'text_align');
		$this->db->update('settings', $data);
	}


	////////private message//////
	function send_new_private_message()
	{
		$message = $this->input->post('message');
		$timestamp = strtotime(date("Y-m-d H:i:s"));

		$reciever = $this->input->post('reciever');
		$sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');

		//check if the thread between those 2 users exists, if not create new thread
		$num1 = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->num_rows();
		$num2 = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->num_rows();

		if ($num1 == 0 && $num2 == 0) {
			$message_thread_code = substr(md5(rand(100000000, 20000000000)), 0, 15);
			$data_message_thread['message_thread_code'] = $message_thread_code;
			$data_message_thread['sender'] = $sender;
			$data_message_thread['reciever'] = $reciever;
			$this->db->insert('message_thread', $data_message_thread);
		}
		if ($num1 > 0)
			$message_thread_code = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->row()->message_thread_code;
		if ($num2 > 0)
			$message_thread_code = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->row()->message_thread_code;


		$data_message['message_thread_code'] = $message_thread_code;
		$data_message['message'] = $message;
		$data_message['sender'] = $sender;
		$data_message['timestamp'] = $timestamp;
		$this->db->insert('message', $data_message);

		// notify email to email reciever
		//$this->email_model->notify_email('new_message_notification', $this->db->insert_id());

		return $message_thread_code;
	}

	function send_reply_message($message_thread_code)
	{
		$message = $this->input->post('message');
		$timestamp = strtotime(date("Y-m-d H:i:s"));
		$sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');


		$data_message['message_thread_code'] = $message_thread_code;
		$data_message['message'] = $message;
		$data_message['sender'] = $sender;
		$data_message['timestamp'] = $timestamp;
		$this->db->insert('message', $data_message);

		// notify email to email reciever
		//$this->email_model->notify_email('new_message_notification', $this->db->insert_id());
	}

	function mark_thread_messages_read($message_thread_code)
	{
		// mark read only the oponnent messages of this thread, not currently logged in user's sent messages
		$current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
		$this->db->where('sender !=', $current_user);
		$this->db->where('message_thread_code', $message_thread_code);
		$this->db->update('message', array('read_status' => 1));
	}

	function count_unread_message_of_thread($message_thread_code)
	{
		$unread_message_counter = 0;
		$current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
		$messages = $this->db->get_where('message', array('message_thread_code' => $message_thread_code))->result_array();
		foreach ($messages as $row) {
			if ($row['sender'] != $current_user && $row['read_status'] == '0')
				$unread_message_counter++;
		}
		return $unread_message_counter;
	}


	////////NEWSLETTER//////////
	function save_newsletter_info()
	{
		$data['timestamp'] = strtotime($this->input->post('timestamp'));
		$data['title'] = $this->input->post('title');
		$data['description'] = $this->input->post('description');
		$data['file_name'] = $_FILES["file_name"]["name"];
		$data['file_type'] = $this->input->post('file_type');
		$this->db->insert('document', $data);

		$document_id = $this->db->insert_id();
		move_uploaded_file($_FILES["file_name"]["tmp_name"], "uploads/document/" . $_FILES["file_name"]["name"]);
	}

	function select_newsletter_info()
	{
		$this->db->order_by("timestamp", "desc");
		return $this->db->get('document')->result_array();
	}


	function update_newsletter_info($document_id)
	{
		$data['timestamp'] = strtotime($this->input->post('timestamp'));
		$data['title'] = $this->input->post('title');
		$data['description'] = $this->input->post('description');
		$data['class_id'] = $this->input->post('class_id');
		$data['subject_id'] = $this->input->post('subject_id');
		$this->db->where('document_id', $document_id);
		$this->db->update('document', $data);
	}

	function delete_newsletter_info($document_id)
	{
		$this->db->where('document_id', $document_id);
		$this->db->delete('document');
	}


}
