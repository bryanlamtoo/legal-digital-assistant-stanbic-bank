<!-- Edit Comment Modal -->

<div class="row">
	<form role="form" method="post">

		<div class="form-group">
			<textarea class="form-control wysihtml5" data-stylesheet-url="assets/css/wysihtml5-color.css" name="sample_wysiwyg" id="sample_wysiwyg"></textarea>
		</div>


		<button type="button" class="btn btn-green btn-icon icon-left">
			Submit
			<i class="entypo-check"></i>
		</button>

	</form>
</div>
