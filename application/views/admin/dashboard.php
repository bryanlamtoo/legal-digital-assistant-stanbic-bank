<!-- Search search form -->


<div class="jumbotron animated zoomIn ">
	<h1>Search Here</h1>

	<p>
		Type your question here or select a category below
	</p>

	<br/>

	<form method="get" class="search-bar" action="" enctype="application/x-www-form-urlencoded">

		<div class="input-group">
			<input id="faq_search" type="text" class="form-control input-lg" name="search" placeholder="Search for something...">

			<div class="input-group-btn">
				<a type="submit" class="btn btn-lg btn-primary btn-icon">
					Search
					<i class="entypo-search"></i>
				</a>
			</div>
		</div>

	</form>


</div>


	<div class="col-sm-4 col-xs-6 animated fadeInDown">

		<div class="tile-stats tile-red">
			<div class="icon"><i class="entypo-users"></i></div>
			<div class="num" data-start="0" data-end="83" data-postfix="" data-duration="1500" data-delay="0">0
			</div>

			<h3>Categories</h3>
			<p>so far in our blog, and our website.</p>
		</div>

	</div>

	<div class="col-sm-4 col-xs-6 animated fadeInDown"">

		<div class="tile-stats tile-green">
			<div class="icon"><i class="entypo-chart-bar"></i></div>
			<div class="num" data-start="0" data-end="135" data-postfix="" data-duration="1500"
				 data-delay="600">0
			</div>

			<h3>FAQ</h3>
			<p>this is the average value.</p>
		</div>

	</div>


	<div class="col-sm-4 col-xs-6 animated fadeInDown"">

		<div class="tile-stats tile-aqua">
			<div class="icon"><i class="entypo-mail"></i></div>
			<div class="num" data-start="0" data-end="23" data-postfix="" data-duration="1500"
				 data-delay="1200">0
			</div>

			<h3>New Messages</h3>
			<p>messages per day.</p>
		</div>

	</div>

<div class="clear visible-xs"></div>

<div class="row">





<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table1 = jQuery( '#table-2' );

		// Initialize DataTable
		$table1.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true,

		});

		$table1.fnFilter('');

		// Initalize Select Dropdown after DataTables is created
		$table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$('#faq_search').keyup(function(){
			$table1.fnFilter($(this).val()) ;
		});

	} );
</script>

	<div class="col-md-9 animated slideInUp">

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
	<tr>
		<th><h3 style="text-align: center">Frequently Asked Questions</h3>
		</th>

		<th>Actions</th>
	</tr>
	</thead>

	<tbody>

	<?php foreach ($question_info as $row) { ?>
					<tr>
						<td>
							<h4><a href="<?php echo base_url() ?>index.php?admin/view_question/<?php echo $row['question_id'] ?>"> <?php echo $row['question']; ?></a>
							</h4>
						</td>
						<td>
							<a href="<?php echo base_url() ?>index.php?admin/view_question/<?php echo $row['question_id'] ?>" type="button" class="btn btn-green btn-icon icon-left">
								View
								<i class="entypo-eye"></i>
							</a>
						</td>
					</tr>

				<?php } ?>




	</tbody>
</table>

</div>
<div class="list-group col-md-3 animated slideInUp"" >
<a href="#" class="list-group-item active">
	Most Popular Categories
</a>
<?php foreach ($question_info as $row){?>



	<a href="#" class="list-group-item">
		<span class="badge badge-info">14</span>
		<?php echo $row['title']?>
	</a>

<?php }?>
</div>

</div>

