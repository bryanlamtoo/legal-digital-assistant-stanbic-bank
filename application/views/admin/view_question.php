<div class="row">
	<div class="col-sm-12">
		<div class="well">
			<h3 class="text-info"><?php echo $question_details->question; ?></h3>
			<!--			<h4>--><?php //echo $question_details->title;?><!--</h4>-->
		</div>
	</div>
</div>

<br/>


<a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseTwo-2"
   class="collapsed btn btn-green btn-icon icon-left btn-lg">
	<?php echo get_phrase('submit_an_answer') ?>
	<i class="entypo-publish"></i>
</a>
<div id="collapseTwo-2" class="panel-collapse collapse">
	<div class="panel-body">
		<form role="form" method="post"
			  action="<?php echo base_url() ?>index.php?admin/answer/create/<? echo $question_details->question_id ?>">

			<div class="form-group">
									<textarea class="form-control wysihtml5"
											  data-stylesheet-url="assets/css/wysihtml5-color.css"
											  name="answer" id="answer"></textarea>
			</div>


			<button type="submit" class="btn btn-green btn-icon icon-left">
				Submit
				<i class="entypo-check"></i>
			</button>


	</div>
</div>


<h3>Answers</h3>
<hr/>

<ul class="cbp_tmtimeline">

	<?php foreach ($answer_details as $row) { ?>

		<li>
			<time class="cbp_tmtime" datetime="2015-11-04T03:45"><span>03:45 AM</span> <span>Today</span></time>

			<div class="cbp_tmicon bg-success">
				<i class="entypo-feather"></i>
			</div>

			<div class="cbp_tmlabel">
				<h2><a href="#">Admin</a></h2>
				<p><?php echo $row['answer'] ?></p>
			</div>
		</li>

	<?php } ?>

</ul>
