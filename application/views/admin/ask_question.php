<style>
	.ms-container .ms-list {
		width: 135px;
		height: 205px;
	}

	.post-save-changes {
		float: right;
	}

	@media screen and (max-width: 789px)
	{
		.post-save-changes {
			float: none;
			margin-bottom: 20px;
		}
	}
</style>

<form method="post" role="form" action="<?php echo base_url()?>/index.php?admin/question/create">

	<!-- Title and Publish Buttons -->
	<div class="row">
		<div class="col-sm-2 post-save-changes">
			<button type="submit" class="btn btn-green btn-lg btn-block btn-icon">
				Publish
				<i class="entypo-check"></i>
			</button>
		</div>

		<div class="col-sm-10 has-success">
			<input type="text" data-validate="required" class="form-control input-lg" name="question_title" placeholder="Title here" />
		</div>
	</div>

	<div class="form-group">
<!--		<label for="subject">--><?php //echo get_phrase('category'); ?><!--:</label>-->
		<br><br>
		<select class="form-control select2" name="reciever" required>

			<option value=""><?php echo get_phrase('select_a_category'); ?></option>
			<optgroup label="<?php echo get_phrase('category'); ?>">
				<?php
				$users = $this->db->get('question_category')->result_array();
				foreach ($users as $row):
					?>

					<option value="user-<?php echo $row['category_id']; ?>">
						<?php echo $row['name']; ?></option>

				<?php endforeach; ?>
			</optgroup>

		</select>
	</div>


	<br />

	<!-- WYSIWYG - Content Editor -->
	<div class="row">
		<div class="col-sm-12">
			<textarea class="form-control wysihtml5" rows="18" data-stylesheet-url="assets/css/wysihtml5-color.css" placeholder="Question here" name="question_content" id="question_content"></textarea>
		</div>
	</div>

	<br />

	<!-- Metaboxes -->
	<div class="row">

		<!-- Metabox :: Tags -->
		<div class="col-sm-12">

			<div class="panel panel-primary" data-collapsed="0">

				<div class="panel-heading">
					<div class="panel-title">
						Tags
					</div>

					<div class="panel-options">
						<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					</div>
				</div>

				<div class="panel-body">

					<p>Add Tags</p>
					<input type="text" value="weekend,friday,happy,awesome,chill,healthy" class="form-control tagsinput" />

				</div>

			</div>

		</div>

	</div>

</form>
