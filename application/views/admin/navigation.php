<div class="sidebar-menu">



	<div class="sidebar-menu-inner">
<!--		<div id="particles-js"></div>-->

		<header class="logo-env">

			<!-- logo -->
			<div class="logo" style="">
				<a href="<?php echo base_url(); ?>">
					<img src="assets/images/logo.jpg" style="max-height:60px;"/>
				</a>
			</div>

			<!-- logo collapse icon -->
			<div class="sidebar-collapse" style="">
				<a href="#" class="sidebar-collapse-icon with-animation">

					<i class="entypo-menu"></i>
				</a>
			</div>

			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation">
					<i class="entypo-menu"></i>
				</a>
			</div>
		</header>


		<div style="border-top:1px solid rgba(69, 74, 84, 0.7);"></div>
		<ul id="main-menu" class="">
			<!-- add class "multiple-expanded" to allow multiple submenus to open -->
			<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->


			<!-- DASHBOARD -->
			<li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
				<a href="<?php echo base_url(); ?>index.php?admin/dashboard">
					<i class="entypo-gauge"></i>
					<span><?php echo get_phrase('dashboard'); ?></span>
				</a>
			</li>

<!--			<li class=" has-sub">-->
<!--				<a href="#">-->
<!--					<i class="entypo-doc-text"></i>-->
<!--					<span class="title">Categories</span>-->
<!--				</a>-->
<!---->
<!--				<ul>-->
<!--					<li>-->
<!--						<a href="#">-->
<!--							<span class="title">BANK AND INSOLVENCY</span>-->
<!--						</a>-->
<!--					</li>-->
<!--					<li>-->
<!--						<a href="#">-->
<!--							<span class="title">LETTERS OF ADMINISTRATION OR GRANT OF PROBATE</span>-->
<!--						</a>-->
<!--					</li>-->
<!--					<li>-->
<!--						<a href="#">-->
<!--							<span class="title">CUSTOMER OF UNSOUND MIND</span>-->
<!--						</a>-->
<!--					</li>-->
<!--					<li>-->
<!--						<a href="#">-->
<!---->
<!--							<span class="title"> INSPECTION ORDERS FROM COURT AND AUTHORITIES</span>-->
<!--						</a>-->
<!--					</li>-->
<!--					<li>-->
<!--						<a href="#">-->
<!--							<span class="title">INCAPACITY OF A CUSTOMER</span>-->
<!--						</a>-->
<!--					</li>-->
<!--					<li>-->
<!--						<a href="#">-->
<!--							<span class="title">BUSINESS TYPES</span>-->
<!--						</a>-->
<!--					</li>-->
<!--					<li>-->
<!--						<a href="#">-->
<!--							<span class="title">ACCOUNT MANDATES</span>-->
<!--						</a>-->
<!--					</li>-->
<!--				</ul>-->
<!--			</li>-->



			<li class=" has-sub">
				<a href="#">
					<i class="entypo-help-circled"></i>
					<span class="title">Questions</span>
				</a>

				<ul>
					<li>
						<a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/question_category">
							<i class="entypo-doc-text-inv"></i>
							<span><?php echo get_phrase('categories'); ?></span>
						</a>
					</li>

					<li class="<?php if ($page_name == 'question') echo 'active'; ?> ">
						<a href="<?php echo base_url(); ?>index.php?admin/question">
							<i class="entypo-pencil"></i>
							<span><?php echo get_phrase('ask_a_question'); ?></span>
						</a>
					</li>

				</ul>
			</li>




			<!-- NEWS LETTER -->
			<li class=" has-sub">
				<a href="#">
					<i class="entypo-book-open"></i>
					<span class="title">Newsletters</span>
				</a>

			<ul>
			<li>
				<a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/newsletter_category">
					<i class="entypo-doc-text-inv"></i>
					<span><?php echo get_phrase('categories'); ?></span>
				</a>
			</li>

			<li>

				<a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/newsletter">
					<i class="entypo-docs"></i>
					<span><?php echo get_phrase('all_newsletter'); ?></span>
				</a></li>

			</ul>
			</li>


			<li class="<?php if ($page_name == 'manage_notice') echo 'active'; ?> ">
				<a href="<?php echo base_url(); ?>index.php?admin/notice">
					<i class="entypo-doc-text-inv"></i>
					<span><?php echo get_phrase('noticeboard'); ?></span>
				</a>
			</li>


			<!-- MESSAGE -->
			<li class="<?php if ($page_name == 'message') echo 'active'; ?> ">
				<a href="<?php echo base_url(); ?>index.php?admin/message">
					<i class="entypo-mail"></i>
					<span><?php echo get_phrase('message'); ?></span>
				</a>
			</li>

			<li class="<?php if ($page_name == 'manage_users') echo 'active'; ?> ">
				<a href="<?php echo base_url(); ?>index.php?admin/users">
					<i class="entypo-users"></i>
					<span><?php echo get_phrase('Manage Users'); ?></span>
				</a>
			</li>

			<!-- SETTINGS -->
			<li class="<?php if ($page_name == 'system_settings' || $page_name == 'manage_language' ||
				$page_name == 'sms_settings'
			) echo 'opened active'; ?> ">
				<a href="#">
					<i class="fa fa-wrench"></i>
					<span><?php echo get_phrase('settings'); ?></span>
				</a>
				<ul>
					<li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
						<a href="<?php echo base_url(); ?>index.php?admin/system_settings">
							<span><i class="fa fa-h-square"></i> <?php echo get_phrase('system_settings'); ?></span>
						</a>
					</li>
					<li class="<?php if ($page_name == 'manage_language') echo 'active'; ?> ">
						<a href="<?php echo base_url(); ?>index.php?admin/manage_language">
							<span><i class="fa fa-globe"></i> <?php echo get_phrase('language_settings'); ?></span>
						</a>
					</li>
					<!--					<li class="-->
					<?php //if ($page_name == 'sms_settings') echo 'active'; ?><!-- ">-->
					<!--						<a href="-->
					<?php //echo base_url(); ?><!--index.php?admin/sms_settings">-->
					<!--							<span><i class="entypo-paper-plane"></i> -->
					<?php //echo get_phrase('sms_settings'); ?><!--</span>-->
					<!--						</a>-->
					<!--					</li>-->
				</ul>
			</li>

			<!-- ACCOUNT -->
			<li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
				<a href="<?php echo base_url(); ?>index.php?admin/manage_profile">
					<i class="entypo-lock"></i>
					<span><?php echo get_phrase('account'); ?></span>
				</a>
			</li>


		</ul>






	</div>
</div>
