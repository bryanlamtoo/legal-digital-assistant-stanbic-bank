<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');


/**
 * Created by IntelliJ IDEA.
 * User: macbookpro
 * Date: 20/07/2017
 * Time: 12:12
 */
class Admin extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');

		/* cache control */
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}

	/*     * *default function, redirects to login page if no admin logged in yet** */

	public function index()
	{
		if ($this->session->userdata('admin_login') != 1)
			redirect(base_url() . 'index.php?login', 'refresh');
		if ($this->session->userdata('admin_login') == 1)
			redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
	}

	/*     * *ADMIN DASHBOARD** */

	function dashboard()
	{

		if ($this->session->userdata('admin_login') != 1) {
			$this->session->set_userdata('last_page', current_url());
			redirect(base_url(), 'refresh');
		}


		$page_data['question_info'] = $this->crud_model->select_question_info();
		$page_data['page_name'] = 'dashboard';
		$page_data['page_title'] = get_phrase('admin_dashboard');

		$this->load->view('index', $page_data);
	}


	/*     * ***LANGUAGE SETTINGS******** */

	function manage_language($param1 = '', $param2 = '', $param3 = '')
	{
		if ($this->session->userdata('admin_login') != 1)
			redirect(base_url() . 'index.php?login', 'refresh');

		if ($param1 == 'edit_phrase') {
			$page_data['edit_profile'] = $param2;
		}
		if ($param1 == 'update_phrase') {
			$language = $param2;
			$total_phrase = $this->input->post('total_phrase');
			for ($i = 1; $i < $total_phrase; $i++) {
				//$data[$language]	=	$this->input->post('phrase').$i;
				$this->db->where('phrase_id', $i);
				$this->db->update('language', array($language => $this->input->post('phrase' . $i)));
			}
			redirect(base_url() . 'index.php?admin/manage_language/edit_phrase/' . $language, 'refresh');
		}
		if ($param1 == 'do_update') {
			$language = $this->input->post('language');
			$data[$language] = $this->input->post('phrase');
			$this->db->where('phrase_id', $param2);
			$this->db->update('language', $data);
			$this->session->set_flashdata('message', get_phrase('settings_updated'));
			redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
		}
		if ($param1 == 'add_phrase') {
			$data['phrase'] = $this->input->post('phrase');
			$this->db->insert('language', $data);
			$this->session->set_flashdata('message', get_phrase('settings_updated'));
			redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
		}
		if ($param1 == 'add_language') {
			$language = $this->input->post('language');
			$this->load->dbforge();
			$fields = array(
				$language => array(
					'type' => 'LONGTEXT'
				)
			);
			$this->dbforge->add_column('language', $fields);

			$this->session->set_flashdata('message', get_phrase('settings_updated'));
			redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
		}
		if ($param1 == 'delete_language') {
			$language = $param2;
			$this->load->dbforge();
			$this->dbforge->drop_column('language', $language);
			$this->session->set_flashdata('message', get_phrase('settings_updated'));

			redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
		}
		$page_data['page_name'] = 'manage_language';
		$page_data['page_title'] = get_phrase('manage_language');
		//$page_data['language_phrases'] = $this->db->get('language')->result_array();
		$this->load->view('index', $page_data);
	}


	/*     * ***SITE/SYSTEM SETTINGS******** */

	function system_settings($param1 = '', $param2 = '', $param3 = '')
	{
		if ($this->session->userdata('admin_login') != 1)
			redirect(base_url() . 'index.php?login', 'refresh');

		if ($param1 == 'do_update') {
			$this->crud_model->update_system_settings();
			$this->session->set_flashdata('message', get_phrase('settings_updated'));
			redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
		}
		if ($param1 == 'upload_logo') {
			move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
			$this->session->set_flashdata('message', get_phrase('settings_updated'));
			redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
		}
		$page_data['page_name'] = 'system_settings';
		$page_data['page_title'] = get_phrase('system_settings');
		$page_data['settings'] = $this->db->get('settings')->result_array();
		$this->load->view('/index', $page_data);
	}


	// SMS settings.
	function sms_settings($param1 = '')
	{

		if ($this->session->userdata('admin_login') != 1)
			redirect(base_url() . 'index.php?login', 'refresh');

		if ($param1 == 'do_update') {
			$this->crud_model->update_sms_settings();
			$this->session->set_flashdata('message', get_phrase('settings_updated'));
			redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
		}

		$page_data['page_name'] = 'sms_settings';
		$page_data['page_title'] = get_phrase('sms_settings');
		$this->load->view('index', $page_data);
	}


	/*     * ****MANAGE OWN PROFILE AND CHANGE PASSWORD** */

	function manage_profile($param1 = '', $param2 = '', $param3 = '')
	{
		if ($this->session->userdata('admin_login') != 1)
			redirect(base_url() . 'index.php?login', 'refresh');

		if ($param1 == 'update_profile_info') {

			$this->crud_model->update_own_info($param2);

			$this->session->set_flashdata('message', get_phrase('profile_info_updated_successfuly'));
			redirect(base_url() . 'index.php?admin/manage_profile');
		}
		if ($param1 == 'change_password') {
			$current_password_input = sha1($this->input->post('password'));
			$new_password = sha1($this->input->post('new_password'));
			$confirm_new_password = sha1($this->input->post('confirm_new_password'));

			$current_password_db = $this->db->get_where('admin', array('admin_id' =>
				$this->session->userdata('login_user_id')))->row()->password;

			if ($current_password_db == $current_password_input && $new_password == $confirm_new_password) {
				$this->db->where('admin_id', $this->session->userdata('login_user_id'));
				$this->db->update('admin', array('password' => $new_password));

				$this->session->set_flashdata('message', get_phrase('password_info_updated_successfuly'));
				redirect(base_url() . 'index.php?admin/manage_profile');
			} else {
				$this->session->set_flashdata('message', get_phrase('password_update_failed'));
				redirect(base_url() . 'index.php?admin/manage_profile');
			}
		}
		$page_data['page_name'] = 'manage_profile';
		$page_data['page_title'] = get_phrase('manage_profile');
		$page_data['edit_data'] = $this->db->get_where('admin', array('admin_id' => $this->session->userdata('login_user_id')))->result_array();
		$this->load->view('index', $page_data);
	}


	function notice($task = "", $notice_id = "")
	{
		if ($this->session->userdata('admin_login') != 1) {
			$this->session->set_userdata('last_page', current_url());
			redirect(base_url(), 'refresh');
		}

		if ($task == "create") {
			$this->crud_model->save_notice_info();
			$this->session->set_flashdata('message', get_phrase('notice_info_saved_successfully'));
			redirect(base_url() . 'index.php?admin/notice');
		}

		if ($task == "update") {
			$this->crud_model->update_notice_info($notice_id);
			$this->session->set_flashdata('message', get_phrase('notice_info_updated_successfully'));
			redirect(base_url() . 'index.php?admin/notice');
		}

		if ($task == "delete") {
			$this->crud_model->delete_notice_info($notice_id);
			redirect(base_url() . 'index.php?admin/notice');
		}

		$data['notice_info'] = $this->crud_model->select_notice_info();
		$data['page_name'] = 'manage_notice';
		$data['page_title'] = get_phrase('noticeboard');
		$this->load->view('index', $data);
	}




	function answer($task ='',$question_id){

		if ($task == 'create'){

			$this->crud_model->save_answer_info($question_id);
			$this->session->set_flashdata('message', get_phrase('answer_saved_successfully'));
			$this->session->set_userdata('last_page', current_url());
//			redirect(base_url(), 'refresh');
			redirect(base_url() . 'index.php?admin/view_question/'.$question_id);


		}
	}

	function question($task = "", $question_id = "")
	{
		if ($this->session->userdata('admin_login') != 1) {
			$this->session->set_userdata('last_page', current_url());
			redirect(current_url(), 'refresh');
		}

		if ($task == 'create') {

			$this->crud_model->save_question_info();
			$this->session->set_flashdata('message', get_phrase('question_added_successfully'));
			redirect(base_url() . 'index.php?admin/dashboard');


		}


		$data['page_name'] = 'ask_question';
		$data['page_title'] = get_phrase('Add New Question');

		$this->load->view('index', $data);
	}

	function question_category($param1 = '',$param2 = '' , $param3 = ''){

		if ($param1 == 'create') {
			$data['name']       = $this->input->post('name');
			$this->db->insert('question_category', $data);
			$this->session->set_flashdata('flash_message' , get_phrase('category_added_successfully'));
			redirect(base_url() . 'index.php?admin/question_category','refresh');
		}

		$data['categories']   = $this->db->get('question_category')->result_array();

		$data['page_name'] = 'question_category';
		$data['page_title'] = 'Manage categories';

		$this->load->view('index',$data);
	}


	function view_question($question_id = "")
	{

		$data['question_details'] = $this->db->get_where('question', array('question_id' => $question_id))->row();
		$data['answer_details'] = $this->db->get_where('answer',array('question_id' => $question_id))->result_array();

//		$data['user_details'] = $this->db->get_where('');
		$data['page_name'] = 'view_question';
		$data['page_title'] = get_phrase('Question');

		$this->load->view('index', $data);
	}


	function users($task = "", $user_id = "")
	{
		if ($this->session->userdata('admin_login') != 1) {
			$this->session->set_userdata('last_page', current_url());
			redirect(base_url(), 'refresh');
		}

		if ($task == "create") {
			$email = $_POST['email'];
			$user = $this->db->get_where('user', array('email' => $email))->row()->name;
			if ($user == null) {
				$this->crud_model->save_user_info();
				$this->session->set_flashdata('message', get_phrase('user_info_saved_successfuly'));
			} else {
				$this->session->set_flashdata('message', get_phrase('duplicate_email'));
			}
			redirect(base_url() . 'index.php?admin/users');
		}

		if ($task == "update") {
			$this->crud_model->update_user_info($user_id);
			$this->session->set_flashdata('message', get_phrase('user_info_updated_successfuly'));
			redirect(base_url() . 'index.php?admin/users');
		}

		if ($task == "delete") {
			$this->crud_model->delete_user_info($user_id);
			redirect(base_url() . 'index.php?admin/users');
		}

		$data['user_info'] = $this->crud_model->select_user_info();
		$data['page_name'] = 'manage_user';
		$data['page_title'] = get_phrase('user');
		$this->load->view('index', $data);
	}

	/* private messaging */

	function message($param1 = 'message_home', $param2 = '', $param3 = '')
	{
		if ($this->session->userdata('admin_login') != 1)
			redirect(base_url(), 'refresh');

		if ($param1 == 'send_new') {
			$message_thread_code = $this->crud_model->send_new_private_message();
			$this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
			redirect(base_url() . 'index.php?admin/message/message_read/' . $message_thread_code, 'refresh');
		}

		if ($param1 == 'send_reply') {
			$this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code
			$this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
			redirect(base_url() . 'index.php?admin/message/message_read/' . $param2, 'refresh');
		}

		if ($param1 == 'message_read') {
			$page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code
			$this->crud_model->mark_thread_messages_read($param2);
		}

		$page_data['message_inner_page_name'] = $param1;
		$page_data['page_name'] = 'message';
		$page_data['page_title'] = get_phrase('private_messaging');
		$this->load->view('index', $page_data);
	}


	/*********MANAGE NEWSLETTER************/
	function newsletter($task = "", $document_id = "")
	{
		if ($this->session->userdata('admin_login') != 1) {
			$this->session->set_userdata('last_page', current_url());
			redirect(base_url(), 'refresh');
		}

		if ($task == "create") {
			$this->crud_model->save_newsletter_info();
			$this->session->set_flashdata('flash_message', get_phrase('newsletter_info_saved_successfuly'));
			redirect(base_url() . 'index.php?admin/newsletter', 'refresh');
		}

		if ($task == "update") {
			$this->crud_model->update_newsletter_info($document_id);
			$this->session->set_flashdata('flash_message', get_phrase('newsletter_info_updated_successfuly'));
			redirect(base_url() . 'index.php?admin/newsletter', 'refresh');
		}

		if ($task == "delete") {
			$this->crud_model->delete_newsletter_info($document_id);
			redirect(base_url() . 'index.php?admin/newsletter');
		}


		$data['newsletter_info'] = $this->crud_model->select_newsletter_info();
		$data['page_name'] = 'newsletter';
		$data['page_title'] = get_phrase('newsletter');

		$this->load->view('index', $data);
	}


	function view_document($document_id = '')
	{

//			$type = $this->db->get_where('document', array('document_id' => $document_id))->row()->file_type;
//			$name = $this->db->get_where('document', array('document_id' => $document_id))->row()->file_name;
//
//			$pdf ='';
//
//			if (file_exists('uploads/ document/' . $name . '.' . $type)) {
//				$doc_url = base_url() . 'uploads/document/' . $name . '.' . $type;
//				$pdf = file_get_contents($doc_url);
//			}
//
//			$len = strlen($pdf);
//			header("Content-type: application/pdf");
//			header("Content-Length:" . $len);
//			header("Content-Disposition: inline; filename=Resume.pdf");
//
		$data['page_name'] = 'view_file';
		$data['page_title'] = 'view_file';
		$data['document_id'] = $document_id;


		$this->load->view('index', $data);
	}


	function generate_pdf($document_id = '')
	{
		$type = $this->db->get_where('document', array('document_id' => $document_id))->row()->file_type;
		$name = $this->db->get_where('document', array('document_id' => $document_id))->row()->file_name;


//		$doc = new Docx_reader();
//		$doc->setFile(base_url() . 'uploads/document/' . $name . '.' . $type);
//
//		$plain_text = $doc->to_plain_text();
//		$html = $doc->to_html();
//
//		$pdf = pdf_create($html, 'testDoc4', false);
//		$len = strlen($pdf);
//		header("Content-type: application/pdf");
//		header("Content-Length:" . $len);
//		header("Content-Disposition: inline; filename=Resume.pdf");

		print file_get_contents(base_url() . 'uploads/document/' . $name . '.' . $type);
	}




}
